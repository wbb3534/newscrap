package com.seop.newsscrap.controller;

import com.seop.newsscrap.model.NewsItem;
import com.seop.newsscrap.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/scrap")
@RequiredArgsConstructor
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<NewsItem> getHtml() throws IOException { //1.명찰을 보고 getHtml에 방문
        List<NewsItem> result = scrapService.run(); // run에가서 작업후 결과물을 다시가져와서 result에 저장
        return result;
    }
}
