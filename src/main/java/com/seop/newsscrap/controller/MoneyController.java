package com.seop.newsscrap.controller;

import com.seop.newsscrap.model.MoneyChangeRequest;
import com.seop.newsscrap.model.VoucherChangeRequest;
import com.seop.newsscrap.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController // api 할 때 필요
@RequestMapping("/money") // 안내데스크에 위에 있는 큰 간판 같은 존재(분류 하기 위한 맵핑)
@RequiredArgsConstructor // 혼자 일 못하게 함 머니서비스랑 일을 같이한다고 명시 함 Rear tap 치면 나옴
public class MoneyController {
    private final MoneyService moneyService;

    @PostMapping("/change")
    public String peopleChange(@RequestBody MoneyChangeRequest request) { // 안내데스크 큰 간판에 속해 있으며 밑에 있는 간판 같은존재 1 ex) 교환안내
        String result = moneyService.convertMoney(request.getMoney());
        return result + "교환되었습니다 고객님";
    }

    @PostMapping("/pay-back") // 케밥케이스로 작성
    public String peoplePayBack(@RequestBody VoucherChangeRequest request) { // 안내데스크 큰 간판에 속해 있으며 밑에 있는 간판 같은존재 2 ex) 환불안내
        String result = moneyService.convertVoucher(request.getVoucher());
        return result + "환불되었습니다 고객님";
    }
}
