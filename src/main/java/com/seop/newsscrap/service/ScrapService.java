package com.seop.newsscrap.service;

import com.seop.newsscrap.model.NewsItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {

    private Document getFullHtml() throws IOException {
        String url = "http://www.astronomer.rocks/";   //문자열에 접속할 주소
        Connection connection = Jsoup.connect(url);    //주소에 접속
        Document document = connection.get(); //get() 빨간줄 뜨는 이유 url이 없을 수 도 있어서 alt + enter 눌러서 trows IOException 추가 , 주소에 접속해서 html긁어옴
        return document;
    }

    private List<Element> parseHtml(Document document) { //문서를 받아서 List<Element>로 주기로 함
        Elements elements = document.getElementsByClass("auto-article"); //문서에서 auto-article이라는 클라스만 가져옴
        List<Element> tempResult = new LinkedList<>();  //결과값을 넣을 상자를 만듬
        for(Element item : elements) {                  // 엘리먼트를 item에 넣음
            Elements lis = item.getElementsByTag("li"); //li를 뽑아옴
            for (Element item2 : lis) {          //lis를 item2에 넣음
                tempResult.add(item2);        // temResult에 item2을 넣음
            }
        }
        return tempResult;
    }

    private List<NewsItem> makeResult(List<Element> list){
        List<NewsItem> result = new LinkedList<>();  //결과값을 넣을 공간을 만듬
        for (Element item : list) {
            Elements checkContents = item.getElementsByClass("flow-hidden");  //flow-hidden클래스를 가져옴
            if (checkContents.size() == 0) {                                            // 만약 가져온 클래스가 0개라면
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB");
                if (checkHiddenBanner.size() > 0) {
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    String contents = item.getElementsByClass("auto-fontB").get(0).text();  //내가원하는애만 가져오기 위해 조건식을 걸어 거름

                    NewsItem addItem = new NewsItem(); //model에 넣음
                    addItem.setTitle(title);
                    addItem.setContents(contents);

                    result.add(addItem);
                }
            }
        }
        return result;
    }

    public List<NewsItem> run() throws IOException {
        Document document = getFullHtml();               //getFullHtml에게 문서를 받아 document에 저장
        List<Element> elements = parseHtml(document);
        List<NewsItem> result = makeResult(elements);

        return result;
    }
}
