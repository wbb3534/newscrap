package com.seop.newsscrap.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VoucherChangeRequest {
    private String voucher;
}
